<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Zonal extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'zonals';


    protected $fillable = [
        'name'
    ];
}
