<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AboutUs extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'about_us';

    public function getNewExecutivesAttribute($value){
        return asset('/images/users/'.$value);
    }

    public function getOldExecutiveAttribute($value){
        return asset('/images/users/'.$value);
    }
}
