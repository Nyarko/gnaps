<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carousel extends Model
{
    protected $table = 'carousels';

    public function getPictureAttribute($value){
        return asset('/images/slider/'.$value);
    }
}
