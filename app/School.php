<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class School extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'schools';

    public static function getSchoolInfo($id){
        $schools = DB::table('schools')
            ->leftJoin('users','schools.user_id','=', 'users.id')
            ->leftJoin('regions','schools.region_id','=', 'regions.id')
            ->leftJoin('districts','schools.district_id','=', 'districts.id')
            ->leftJoin('zonals','schools.zone_id','=', 'zonals.id')
            ->select('schools.*','users.userID','users.first_name','users.other_name','users.contact as userContact',
                'users.email as userEmail','regions.name as regionName','districts.name as districtName',
                'zonals.name as zoneName')
            ->where('schools.id',$id)
            ->where('schools.deleted_at', null)
            ->first();

        return $schools;
    }

    public static function getSchoolClass($id){
        $students = DB::table('statistics')
            ->leftJoin('classes','statistics.class_id','=','classes.id')
            ->select('statistics.*','classes.name as clasName')
            ->where('statistics.school_id', $id)
            ->where('statistics.deleted_at',null)
            ->get();

        return $students;
    }

    public static function getSchoolHead($id){
        $head_master = HeadMaster::where('school_id',$id)
                    ->where('deleted_at',null)
                    ->get();
        return $head_master;
    }

    public static function getSchoolRegister($id){
        $register = SchoolRegister::where('school_id',$id)
            ->where('deleted_at', null)
            ->first();
        return $register;
    }

    public static function getSchoolLevel($id) {
        $classLevels = DB::table('school_classes')
                        ->leftJoin('class_levels','school_classes.level_id','=','class_levels.id')
                        ->select('class_levels.*')
                        ->where('school_classes.school_id',$id)
                        ->where('school_classes.deleted_at',null)
                        ->get();
        return $classLevels;
    }

    public static function getSchoolBook($id){
        $books = DB::table('books')
                ->leftJoin('classes','books.class_id','=','classes.id')
                ->leftJoin('subjects','books.subject_id','=','subjects.id')
                ->select('books.*','classes.name','subjects.subject')
                ->where('books.school_id',$id)
                ->where('books.deleted_at',null)
                ->get();

        return $books;
    }
}
