<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Advert extends Model
{
        use SoftDeletes;
        protected $dates = ['deleted_at'];

        protected $table = 'adverts';

        public function getBannerAttribute($value){
            return asset('/images/advert/'.$value);
        }
}
