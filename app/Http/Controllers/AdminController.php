<?php

namespace App\Http\Controllers;

use App\AboutUs;
use App\Advert;
use App\Carousel;
use App\Classes;
use App\ClassLevel;
use App\Comment;
use App\District;
use App\Gallery;
use App\HeadMaster;
use App\Post;
use App\Region;
use App\Report;
use App\School;
use App\SchoolRegister;
use App\Statistic;
use App\Subject;
use App\User;
use App\Zonal;
use App\Message;
use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{

    //=======================================
    //  The Pages Function
    //=======================================
    public function index(){
        $allschool = School::all()->count();
        $schoolGnaps = School::where('status',1)->get()->count();
        $schoolNonGnaps = School::where('status',0)->get()->count();
        $allusers = User::all()->count();
        $allexecutive = User::where('user_type','Executive')->get()->count();
        $allmembers = User::where('user_type','Member')->get()->count();
        $allregions = Region::all()->count();
        $alldistrict = District::all()->count();
        $allzone = Zonal::all()->count();
        $allpost = Post::all()->count();
        $allcomment = Comment::all()->count();
        $allmessage = Message::all()->count();
        $allgallery = Gallery::all()->count();
        $allgirlstudent = Statistic::select('enroll_girls')->sum('enroll_girls');
        $allboystudent = Statistic::select('enroll_boys')->sum('enroll_boys');
        $allstudents = $allboystudent + $allgirlstudent;
        $alltrainedmale = Statistic::select('male_trained')->sum('male_trained');
        $allustrainedmale = Statistic::select('male_untrained')->sum('male_untrained');
        $alltrainedfemale = Statistic::select('female_trained')->sum('female_trained');
        $alluntrainedfemale = Statistic::select('female_untrained')->sum('female_untrained');
        $allteachers = $alltrainedmale + $allustrainedmale + $alltrainedfemale + $alluntrainedfemale;


       return view('Admin.index', compact('allschool','schoolGnaps','schoolNonGnaps','allusers',
           'allexecutive','allmembers','allregions','alldistrict','allzone','allpost','allcomment','allmessage',
           'allgallery','allgirlstudent','allboystudent','allstudents','alltrainedmale','allustrainedmale','alltrainedfemale',
           'alluntrainedfemale','allteachers'));
    }

    public function executive(){
       return view('Admin.executive');
    }

    public function region(){
        return view('Admin.regions');
    }

    public function district(){
        return view('Admin.districts');
    }

    public function zonals(){
        return view('Admin.zonals');
    }

    public function post(){
        return view('Admin.posts');
    }

    public function message(){
        return view('Admin.message');
    }

    public function classes(){
        return view('Admin.classes');
    }

    public function subject(){
        return view('Admin.subject');
    }

    public function level(){
        return view('Admin.level');
    }

    public function aboutus(){
        return view('Admin.aboutus');
    }

    public function carousel(){
        return view('Admin.carousel');
    }

    public function schoolOwner(){
        return view('Admin.owners');
    }

    public function allSchool(){
        return view('Admin.allschools');
    }

    public function addSchool(){
        return view('Admin.registerschools');
    }

    public function addReport(){
        return view('Admin.report');
    }

    public function generateReport(){
        return view('Admin.generate');
    }

    public function viewSchoolDetail($id){
        return view('Admin.detailsschools', compact('id'));
    }

    public function advert(){
        return view('Admin.advert');
    }


    //======================================
    // Getting Records
    //======================================
    public function getUsers(){
        $user = User::where('deleted_at', null)
                ->where('user_type','Executive')
                ->orderBy('created_at','desc')
                ->get();
        return json_encode($user);
    }

    public function getRegion(){
        $region = Region::where('deleted_at', null)
                  ->orderBy('name','ASC')
                  ->get();

        return json_encode($region);
    }

    public function getDistrict(){
        $district = DB::table('districts')
                    ->leftJoin('regions','districts.region_id','=', 'regions.id')
                    ->select('districts.*','regions.name as region')
                    ->where('districts.deleted_at', null)
                    ->orderBy('regions.name','ASC')
                    ->get();

        return json_encode($district);
    }

    public function getZonal(){
        $zonal = DB::table('zonals')
                 ->leftJoin('districts','zonals.district_id','=','districts.id')
                 ->select('zonals.*','districts.name as district')
                 ->where('zonals.deleted_at', null)
                 ->orderBy('districts.name','ASC')
                 ->get();

        return json_encode($zonal);
    }

    public function getPost(){
        $post = Post::where('deleted_at', null)->orderBy('created_at','desc')->get();
        return json_encode($post);
    }

    public function getComment($id){
        $post_comments = Comment::where('deleted_at',null)
                         ->where('post_id', $id)
                         ->get();
        return json_encode($post_comments);
    }

    public function getGallery($id){
        $post_gallery = Gallery::where('deleted_at', null)
                        ->where('post_id', $id)
                        ->get();

        return json_encode($post_gallery);
    }

    public function getMessage(){
        $message = Message::where('deleted_at',null)
                   ->orderBy('status','ASC')
                   ->get();

        return json_encode($message);
    }

    public function getClasses(){
        $classes = Classes::where('deleted_at', null)
            ->orderBy('name','ASC')
            ->get();
        return json_encode($classes);
    }

    public function getSubject(){
        $subject = Subject::where('deleted_at', null)
            ->orderBy('subject','ASC')
            ->get();

        return json_encode($subject);
    }

    public function getLevel(){
        $level = ClassLevel::where('deleted_at', null)
                ->orderBy('name','ASC')
                ->get();

        return json_encode($level);
    }

    public function getAbout(){
        $about = AboutUs::where('deleted_at', null)
                ->get();

        return json_encode($about);
    }

    public function getCarousel(){
        $carousel = Carousel::orderBy('created_at','desc')->get();

        return json_encode($carousel);
    }

    public function getMember(){
        $user = User::where('deleted_at', null)
                ->where('user_type','Member')
                ->orderBy('created_at','desc')
                ->get();
        return json_encode($user);
    }

    public function getSchools(){
        $schools = DB::table('schools')
                   ->leftJoin('regions', 'schools.region_id','=', 'regions.id')
                   ->leftJoin('districts', 'schools.district_id','=', 'districts.id')
                   ->leftJoin('zonals', 'schools.zone_id','=', 'zonals.id')
                   ->leftJoin('users','schools.user_id','=','users.id')
                   ->select('schools.*','regions.name as region','districts.name as district','zonals.name as zone',
                       'users.first_name', 'users.other_name','users.contact as user_contact','users.userID')
                   ->where('schools.deleted_at',null)
                   ->where('users.deleted_at',null)
                   ->where('regions.deleted_at',null)
                   ->where('districts.deleted_at',null)
                   ->where('zonals.deleted_at',null)
                   ->orderBy('schools.status','asc')
                   ->orderBy('schools.created_at','desc')
                   ->get();

        return json_encode($schools);
    }

    public function getAllUser(){
        $users = User::select(DB::raw("CONCAT(userID, ' | ', other_name,' ', first_name) AS fullname"),'id','userID')
                 ->where('deleted_at', null)
                 ->orderBy('created_at', 'desc')
                 ->get();

        return json_encode($users);
    }

    public function getAllReport(){
        $report = Report::where('deleted_at', null)
                  ->orderBy('created_at', 'desc')
                  ->get();

        return json_encode($report);
    }

    public function getAdvert(){
        $advert = Advert::where('deleted_at',null)
                  ->orderBy('created_at','desc')
                  ->get();

        return json_encode($advert);
    }

    public function getMemberSchool($id){
        $schools = School::where('user_id', $id)->where('deleted_at', null)->select('school_name')->get();
        return json_encode($schools);
    }



    //==============================================
    //   Save Records
    //==============================================
    public function saveUsers(Request $request){
        $validate = Validator::make($request->all(), [
            'firstName' => 'required|string|min:2',
            'otherName' => 'required|string|min:2',
            'email' => 'email|unique:users,email|nullable',
            'contact' => 'required',
            'password' => 'required|min:6',
            'confirm_password' => 'required|min:6|same:password',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        //generate staff #
        $randomNumber = 'GNAPSASR'.str_pad(mt_rand(1, 9999), 4, '0', STR_PAD_LEFT).date('y');


        //checking if staff # already exist in the school
        $foundStaff = DB::table('users')
                     ->where('userID', '=', $randomNumber)
                     ->first();

        // if staff # already exists in the database, create a new one
        while($foundStaff != null) {
            $randomNumber = 'GNAPSASR'.str_pad(mt_rand(1, 9999), 4, '0', STR_PAD_LEFT).date('y');
        }


        try{

            $user = new User();
            $user->userID = $randomNumber;
            $user->first_name = $request['firstName'];
            $user->other_name = $request['otherName'];
            $user->contact = $request['contact'];
            $user->position = $request['position'];
            $user->email = $request['email'];
            $user->is_admin = $request['isAdmin'];
            $user->user_type = 'Executive';
            $user->password = bcrypt($request['password']);

            if($request->hasFile('image')){
                $picture  = $request->file('image');
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->save( public_path('images/users/' . $filename));
                $user->image = $filename;
            } else {
                $user->image = 'avatar.png';
            }

            $user->save();

        }catch (\Exception $e){
            return response()->json(['error' => $e],402);
        }

        return "saved";
    }

    public function saveRegion(Request $request){
        $validate = Validator::make($request->all(), [
            'region' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => 'Filled required field'],422);
        }

        try {

            for ($i=0; $i < count($request['region']); $i++) {

                $dataExist = DB::table('regions')
                    ->select('name')
                    ->where('name', $request['region'][$i])
                    ->first();

                if($dataExist){
                    return response()->json(['error' => $dataExist->name.' has been created already'],401);
                }

                $region = new Region();
                $region->name = $request['region'][$i];
                $region->save();
            }

            return "saved";

        } catch (\Exception $e) {
            return response()->json(['error' => 'Check Internet Connection'],402);
        }
    }

    public function saveDistrict(Request $request){
        $validate = Validator::make($request->all(), [
            'region' => 'required',
            'name' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => 'Filled required field'],422);
        }

        try {

            for ($i = 0; $i < count($request['name']); $i++) {

                $dataExist = DB::table('districts')
                            ->select('name','region_id')
                            ->where('name', $request['name'][$i])
                            ->where('region_id', $request['region'][$i])
                            ->first();

                if ($dataExist) {
                    return response()->json(['error' => $dataExist->name . ' has been created for selected region'],
                        401);
                }

                $district = new District();
                $district->name = $request['name'][$i];
                $district->region_id = $request['region'][$i];
                $district->save();
            }

            return "saved";
        } catch (\Exception $e) {
            return response()->json(['error' => 'Check Internet Connection'],402);
        }
    }

    public function saveZonal(Request $request){
        $validate = Validator::make($request->all(), [
            'district' => 'required',
            'name' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => 'Filled required field'],422);
        }

        try {

            for ($i = 0; $i < count($request['name']); $i++) {

                $dataExist = DB::table('zonals')
                            ->select('name','district_id')
                            ->where('name', $request['name'][$i])
                            ->where('district_id', $request['district'][$i])
                            ->first();

                if ($dataExist) {
                    return response()->json(['error' => $dataExist->name . ' has been created for selected district'],
                        401);
                }

                $zonal = new Zonal();
                $zonal->name = $request['name'][$i];
                $zonal->district_id = $request['district'][$i];
                $zonal->save();
            }

            return "saved";
        } catch (\Exception $e) {
            return response()->json(['error' => 'Check Internet Connection'],402);
        }
    }

    public function savePost(Request $request){
        $validate = Validator::make($request->all(), [
            'title' => 'required|string|min:2',
            'content' => 'required|string|min:2',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        try{

            if ($request['created_at']) {
               $posted_date = $request['created_at'];
            } else {
               $posted_date = now()->format("yy-m-d");
            }

            $post = new Post();
            $post->title = $request['title'];
            $post->body = $request['content'];
            $post->posted_on = $posted_date;

            if($request->hasFile('image')){
                $picture  = $request->file('image');
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->save( public_path('images/gallery/' . $filename));
                $post->image = $filename;
            }

            $post->save();

            $post_id = $post->id;

            if ($request->hasFile('gallery')){
                foreach ($request->gallery as $img){
                    $gallery = new Gallery();
                    $poster = $img;
                    $filename = time().$poster->getClientOriginalName();
                    Image::make($poster)->save(public_path('/images/gallery/'.$filename));

                    $gallery->gallery = $filename;
                    $gallery->post_id = $post_id;
                    $gallery->save();
                }
            }

        }catch (\Exception $e){
            return response()->json(['error' => $e],402);
        }

        return "saved";
    }

    public function saveClasses(Request $request){
        $validate = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => 'Filled required field'],422);
        }

        try {

            for ($i=0; $i < count($request['name']); $i++) {

                $dataExist = DB::table('classes')
                            ->select('name')
                            ->where('name', $request['name'][$i])
                            ->first();

                if($dataExist){
                    return response()->json(['error' => $dataExist->name.' has been created already'],401);
                }

                $classes = new Classes();
                $classes->name = $request['name'][$i];
                $classes->save();
            }

            return "saved";

        } catch (\Exception $e) {
            return response()->json(['error' => 'Check Internet Connection'],402);
        }
    }

    public function saveSubject(Request $request){
        $validate = Validator::make($request->all(), [
            'subject' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => 'Filled required field'],422);
        }

        try {

            for ($i=0; $i < count($request['subject']); $i++) {

                $dataExist = DB::table('subjects')
                            ->select('subject')
                            ->where('subject', $request['subject'][$i])
                            ->first();

                if($dataExist){
                    return response()->json(['error' => $dataExist->subject.' has been created already'],401);
                }

                $subject = new Subject();
                $subject->subject = $request['subject'][$i];
                $subject->save();
            }

            return "saved";

        } catch (\Exception $e) {
            return response()->json(['error' => 'Check Internet Connection'],402);
        }
    }

    public function saveLevel(Request $request){
        $validate = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => 'Filled required field'],422);
        }

        try {

            for ($i=0; $i < count($request['name']); $i++) {

                $dataExist = DB::table('class_levels')
                            ->select('name')
                            ->where('name', $request['name'][$i])
                            ->first();

                if($dataExist){
                    return response()->json(['error' => $dataExist->name.' has been created already'],401);
                }

                $level = new ClassLevel();
                $level->name = $request['name'][$i];
                $level->save();
            }

            return "saved";

        } catch (\Exception $e) {
            return response()->json(['error' => 'Check Internet Connection'],402);
        }
    }

    public function saveAbout(Request $request){
        $validate = Validator::make($request->all(), [
            'about' => 'required|string',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }


        try{

            $about = new AboutUs();
            $about->about = $request['about'];

            if($request->hasFile('new_exe')){
                $picture  = $request->file('new_exe');
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->save( public_path('images/users/' . $filename));
                $about->new_executives = $filename;
            }

            if($request->hasFile('old_exe')){
                $picture  = $request->file('old_exe');
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->save( public_path('images/users/' . $filename));
                $about->old_executive = $filename;
            }

            $about->save();

        }catch (\Exception $e){
            return response()->json(['error' => $e],402);
        }

        return "saved";
    }

    public function saveCarousel(Request $request){
        $validate = Validator::make($request->all(), [
            'title' => 'required|string',
            'picture' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }


        try{

            $status = Carousel::count();
            $carousel = new Carousel();
            $carousel->title = $request['title'];
            $carousel->count = $status + 1;

            if($request->hasFile('picture')){
                $picture  = $request->file('picture');
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->fit(1024,683)->save( public_path('images/slider/' . $filename));
                $carousel->picture = $filename;
            }

            $carousel->save();

        }catch (\Exception $e){
            return response()->json(['error' => $e],402);
        }

        return "saved";
    }

    public function saveSchoolOwner(Request $request){
        $validate = Validator::make($request->all(), [
            'firstName' => 'required|string|min:2',
            'otherName' => 'required|string|min:2',
            'email' => 'email|unique:users,email|nullable',
            'contact' => 'required',
            'position' => 'required|string'
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        //generate staff #
        $randomNumber = 'GNAPSASR'.str_pad(mt_rand(1, 9999), 4, '0', STR_PAD_LEFT).date('y');


        //checking if staff # already exist in the school
        $foundStaff = DB::table('users')
            ->where('userID', '=', $randomNumber)
            ->first();

        // if staff # already exists in the database, create a new one
        while($foundStaff != null) {
            $randomNumber = 'GNAPSASR'.str_pad(mt_rand(1, 9999), 4, '0', STR_PAD_LEFT).date('y');
        }


        try{

            $user = new User();
            $user->userID = $randomNumber;
            $user->first_name = $request['firstName'];
            $user->other_name = $request['otherName'];
            $user->contact = $request['contact'];
            $user->position = 'School Owner';
            $user->email = $request['email'];
            $user->is_admin = 'Member';
            $user->user_type = 'Member';
            $user->password = bcrypt($request['firstName']);

            if($request->hasFile('image')){
                $picture  = $request->file('image');
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->save( public_path('images/users/' . $filename));
                $user->image = $filename;
            } else {
                $user->image = 'avatar.png';
            }

            $user->save();

            return 'saved';

        } catch (\Exception $e) {
            return response()->json(['error' => $e],402);
        }
    }

    public function saveReport(Request $request){
        $validate = Validator::make($request->all(), [
            'title' => 'required|string',
            'content' => 'required|string',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        try {
            $report = new Report();
            $report->title = $request['title'];
            $report->content = $request['content'];
            $report->save();

            return "saved";

        } catch (\Exception $e) {
            return response()->json(['error' => $e],402);
        }
    }

    public function saveAdvert(Request $request){
        $validate = Validator::make($request->all(), [
            'title' => 'required|string|min:2',
            'banner' => 'required',
            'description' => 'required|string|min:2',
            'link' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        try{

            $advert = new Advert();
            $advert->title = $request['title'];
            $advert->description = $request['description'];
            $advert->link = $request['link'];

            if($request->hasFile('banner')){
                $picture  = $request->file('banner');
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->fit(200, 200)->save( public_path('images/advert/' . $filename));
                $advert->banner = $filename;
            }

            $advert->save();


        }catch (\Exception $e){
            return response()->json(['error' => $e],402);
        }

        return "saved";
    }



    //==============================================
    //  Update Records
    //==============================================
    public function updateUsers(Request $request){
        $validate = Validator::make($request->all(), [
            'firstName' => 'required|string|min:2',
            'otherName' => 'required|string|min:2',
            'email' => 'email|required|sometimes',
            'contact' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        try{

            $id = $request['id'];
            $user = User::findOrFail($id);
            $user->first_name = $request['firstName'];
            $user->other_name = $request['otherName'];
            $user->email = $request['email'];
            $user->contact = $request['contact'];
            $user->position = $request['position'];
            $user->is_admin = $request['isAdmin'];

            if($request->hasFile('image')){
                $picture  = $request->file('image');
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->fit(400,400)->save( public_path('images/users/' . $filename));
                $user->image = $filename;
            }

            $user->update();

        }catch (\Exception $e){
            return response()->json(['error' => $e],402);
        }

        return "updated";
    }

    public function updateRegion(Request $request){
        $validate = Validator::make($request->all(), [
            'name' => 'required|string',
            'id' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        try{

            $id = $request['id'];
            $region = Region::findOrFail($id);
            $region->name = $request['name'];
            $region->update();

        }catch (\Exception $e){
            return response()->json(['error' => $e],402);
        }

        return "updated";
    }

    public function updateDistrict(Request $request){
        $validate = Validator::make($request->all(), [
            'name' => 'required|string',
            'region' => 'required',
            'id' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        try{

            $id = $request['id'];
            $district = District::findOrFail($id);
            $district->name = strtolower($request['name']);
            $district->region_id = strtolower($request['region']);
            $district->update();

        }catch (\Exception $e){
            return response()->json(['error' => $e],402);
        }

        return "updated";
    }

    public function updateZonal(Request $request){
        $validate = Validator::make($request->all(), [
            'name' => 'required|string',
            'district' => 'required',
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        try{
            $id = $request['id'];
            $zonal = Zonal::findOrFail($id);
            $zonal->district_id = $request['district'];
            $zonal->name = strtolower($request['name']);
            $zonal->update();

        }catch (\Exception $e){
            return response()->json(['error' => $e],402);
        }

        return "updated";
    }

    public function updatePost(Request $request){
        $validate = Validator::make($request->all(), [
            'title' => 'required|string|min:2',
            'content' => 'required|string|min:2',
            'id' => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        $id = $request['id'];

        try{

            if ($request['created_at']) {
                $posted_date = $request['created_at'];
            } else {
                $posted_date = now()->format("yy-m-d");
            }

            $post = Post::findOrFail($id);
            $post->title = $request['title'];
            $post->body = $request['content'];
            $post->posted_on = $posted_date;

            if($request->hasFile('image')){
                $picture  = $request->file('image');
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->fit(200,200)->save( public_path('images/gallery/' . $filename));
                $post->image = $filename;
            }

            $post->update();

            if ($request->hasFile('gallery')){
                foreach ($request->gallery as $img){
                    $gallery = new Gallery();
                    $poster = $img;
                    $filename = time().$poster->getClientOriginalName();
                    Image::make($poster)->fit(200,200)->save(public_path('/images/gallery/'.$filename));

                    $gallery->gallery = $filename;
                    $gallery->post_id = $id;
                    $gallery->save();
                }
            }

        }catch (\Exception $e){
            return response()->json(['error' => $e],402);
        }

        return "saved";
    }

    public function updateMessage($id){
        $message = Message::findOrFail($id);
        $message->status = 1;
        $message->update();

        return 'update';
    }

    public function updateProfile(Request $request){
        $validate = Validator::make($request->all(), [
            'id' => 'required',
            'firstName' => 'required|string',
            'otherName' => 'required|string',
            'email' => 'required|email',
            'contact' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        $id = $request['id'];
        $admin = User::findOrFail($id);
        $admin->first_name = $request['firstName'];
        $admin->other_name = $request['otherName'];
        $admin->contact = $request['contact'];
        $admin->email = $request['email'];

        if($request->hasFile('image')){
            $profile = $request->file('image');
            $filename = time().$profile->getClientOriginalName();
            Image::make($profile)->save( public_path('images/users/' . $filename));
            $admin->image = $filename;
        }

        $admin->save();

        return 'updated';
    }

    public function updateChangePassword(Request $request){
        $validate = Validator::make($request->all(), [
            'id' => 'required',
            'password' => 'required|min:6',
            'confirm_password' => 'required|min:6|same:password',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        $id = $request['id'];
        $admin = User::findOrFail($id);
        $admin->password = bcrypt($request['password']);
        $admin->save();

        return 'password changed';
    }

    public function updateClasses(Request $request){
        $validate = Validator::make($request->all(), [
            'name' => 'required|string',
            'id' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        try{

            $id = $request['id'];
            $classes = Classes::findOrFail($id);
            $classes->name = $request['name'];
            $classes->update();

        }catch (\Exception $e){
            return response()->json(['error' => $e],402);
        }

        return "updated";
    }

    public function updateSubject(Request $request){
        $validate = Validator::make($request->all(), [
            'subject' => 'required|string',
            'id' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        try{

            $id = $request['id'];
            $subject = Subject::findOrFail($id);
            $subject->subject = $request['subject'];
            $subject->update();

        } catch (\Exception $e){
            return response()->json(['error' => $e],402);
        }

        return "updated";
    }

    public function updateLevel(Request $request){
        $validate = Validator::make($request->all(), [
            'name' => 'required|string',
            'id' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        try{

            $id = $request['id'];
            $level = ClassLevel::findOrFail($id);
            $level->name = $request['name'];
            $level->update();

        }catch (\Exception $e){
            return response()->json(['error' => $e],402);
        }

        return "updated";
    }

    public function updateAbout(Request $request){
        $validate = Validator::make($request->all(), [
            'about' => 'required|string',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }


        try{

            $id = $request['id'];
            $about = AboutUs::findOrFail($id);
            $about->about = $request['about'];

            if($request->hasFile('new_exe')){
                $picture  = $request->file('new_exe');
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->save( public_path('images/users/' . $filename));
                $about->new_executives = $filename;
            }

            if($request->hasFile('old_exe')){
                $picture  = $request->file('old_exe');
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->save( public_path('images/users/' . $filename));
                $about->old_executive = $filename;
            }

            $about->update();

        }catch (\Exception $e){
            return response()->json(['error' => $e],402);
        }

        return "updated";
    }

    public function updateCarousel(Request $request){
        $validate = Validator::make($request->all(), [
            'id' => 'required',
            'title' => 'required|string',
            'picture' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }


        try{

            $id = $request['id'];
            $carousel = Carousel::findOrFail($id);
            $carousel->title = $request['title'];

            if($request->hasFile('picture')){
                $picture  = $request->file('picture');
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->fit(1024,683)->save( public_path('images/slider/' . $filename));
                $carousel->picture = $filename;
            }

            $carousel->save();

        }catch (\Exception $e){
            return response()->json(['error' => $e],402);
        }

        return "saved";
    }

    public function updateSchoolOwner(Request $request){
        $validate = Validator::make($request->all(), [
            'firstName' => 'required|string|min:2',
            'otherName' => 'required|string|min:2',
            'email' => 'required|email',
            'contact' => 'required',
            'id' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }


        try {

            $id = $request['id'];
            $user = User::findOrFail($id);
            $user->first_name = $request['firstName'];
            $user->other_name = $request['otherName'];
            $user->contact = $request['contact'];
            $user->email = $request['email'];

            if($request->hasFile('image')){
                $picture  = $request->file('image');
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->save( public_path('images/users/' . $filename));
                $user->image = $filename;
            }

            $user->save();

            return 'updated';

        } catch (\Exception $e) {
            return response()->json(['error' => $e],402);
        }
    }

    public function updateSchoolMember($id){
        $school = School::findOrFail($id);
        $school->status = 1;
        $school->update();

        return 'update';
    }

    public function updateSchoolGnaps($id){
        $school = School::findOrFail($id);
        $school->status = 0;
        $school->update();

        return 'update';
    }

    public function updateSchool(Request $request){
        $validate = Validator::make($request->all(), [
            'id' => 'required',
            'school_name' => 'required|string|min:2',
            'school_contact' => 'required|string',
            'school_address' => 'required|string',
            'school_facility' => 'required|string',
            'established_date' => 'required|string',
            'region' => 'required|string',
            'district' => 'required|string',
            'town' => 'required|string',
            'suburb' => 'required|string',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        try{

            $id = $request['id'];
            $school = School::findOrFail($id);
            $school->school_name = $request['school_name'];
            $school->email = $request['school_email'];
            $school->contact = $request['school_contact'];
            $school->address = $request['school_address'];
            $school->town = $request['town'];
            $school->suburb = $request['suburb'];
            $school->facilities = $request['school_facility'];
            $school->website_url = $request['school_website'];
            $school->date_established = $request['established_date'];
            $school->region_id = $request['region'];
            $school->district_id = $request['district'];
            $school->zone_id = $request['zone'];

            if($request->hasFile('logo')) {
                $picture  = $request->file('logo');
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->save( public_path('images/schools/' . $filename));
                $school->logo = $filename;
            }

            $school->save();

            return 'updated';

        } catch (\Exception $e) {
            return response()->json(['error' => $e],402);
        }
    }

    public function updateReport(Request $request){
        $validate = Validator::make($request->all(), [
            'title' => 'required|string',
            'content' => 'required|string',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        try {
            $id = $request['id'];
            $report = Report::findOrFail($id);
            $report->title = $request['title'];
            $report->content = $request['content'];
            $report->update();

            return "saved";

        } catch (\Exception $e) {
            return response()->json(['error' => $e],402);
        }
    }

    public function updateStatisticClasses(Request $request){
        $validate = Validator::make($request->all(), [
            'classID' => 'required',
            'classroom' => 'required',
            'boys' => 'required',
            'girls' => 'required',
            'male_trained' => 'required',
            'male_untrained' => 'required',
            'female_trained' => 'required',
            'female_untrained' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        try {
                $id = $request['id'];
                $statistic = Statistic::findOrFail($id);
                $statistic->class_id = $request['classID'];
                $statistic->classrooms = $request['classroom'];
                $statistic->enroll_boys = $request['boys'];
                $statistic->enroll_girls = $request['girls'];
                $statistic->male_trained = $request['male_trained'];
                $statistic->male_untrained = $request['male_untrained'];
                $statistic->female_trained = $request['female_trained'];
                $statistic->female_untrained = $request['female_untrained'];
                $statistic->update();

            return 'updated';

        } catch (\Exception $e) {
            return response()->json(['error' => $e],402);
        }
    }

    public function updateSchoolHead(Request $request){
        $validate = Validator::make($request->all(), [
            'head_name' => 'required',
            'head_contact' => 'required',
            'class_form' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }


        try {

            $id = $request['id'];
            $head_master = HeadMaster::findOrFail($id);
            $head_master->head_name = $request['head_name'];
            $head_master->contact = $request['head_contact'];
            $head_master->class_form = $request['class_form'];
            $head_master->update();

            return 'updated';

        } catch (\Exception $e) {
            return response()->json(['error' => 'Something want wrong'],402);
        }
    }

    public function updateSchoolRegister(Request $request){
        $validate = Validator::make($request->all(), [
            'reg_min' => 'required',
            'reg_gen' => 'required',
            'reg_gnaps' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        try {

            $id = $request['id'];
            $register = SchoolRegister::findOrFail($id);
            $register->reg_with_min_eduction = $request['reg_min'];
            $register->reg_min_number = $request['min_number'];
            $register->registerar_general = $request['reg_gen'];
            $register->reg_gen_number = $request['gen_number'];
            $register->reg_with_gnaps = $request['reg_gnaps'];
            $register->gnaps_number = $request['gnaps_number'];
            $register->update();

            return 'update';

        } catch (\Exception $e) {
            return response()->json(['error' => 'Something went wrong'],402);
        }
    }

    public function updateSchoolTextBook(Request $request){
        $validate = Validator::make($request->all(), [
            'id' => 'required',
            'classID' => 'required',
            'subjectID' => 'required',
            'totalBooks' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        try {
            
            $id = $request['id'];
            $book = Book::findOrFail($id);
            $book->class_id = $request['classID'];
            $book->subject_id = $request['subjectID'];
            $book->number = $request['totalBooks'];
            $book->update();
          
            return 'saved';

        } catch (\Exception $e) {
            return response()->json(['error' => 'Something went wrong'],402);
        }
    }

    public function updateAdvert(Request $request){
        $validate = Validator::make($request->all(), [
            'title' => 'required|string|min:2',
            'banner' => 'required',
            'description' => 'required|string|min:2',
            'link' => 'required',
            'id' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        try{

            $id = $request['id'];
            $advert = Advert::findOrFail($id);
            $advert->title = $request['title'];
            $advert->description = $request['description'];
            $advert->link = $request['link'];

            if($request->hasFile('banner')){
                $picture  = $request->file('banner');
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->save( public_path('images/advert/' . $filename));
                $advert->banner = $filename;
            }

            $advert->update();


        }catch (\Exception $e){
            return response()->json(['error' => $e],402);
        }

        return "updated";
    }




    //=============================================
    //  Deleting User
    //=============================================
    public function deleteUser($id){
         $user = User::findOrFail($id);
         $user->delete();

        return 'deleted';
    }

    public function deleteRegion($id){
        $region = Region::findOrFail($id);
        $region->delete();
        return 'deleted';
    }

    public function deleteDistrict($id){
        $district = District::findOrFail($id);
        $district->delete();

        return 'deleted';
    }

    public function deleteZonal($id){
        $zonal = Zonal::findOrFail($id);
        $zonal->delete();

        return 'deleted';
    }

    public function deletePost($id){
        Comment::where('post_id', $id)->delete();
        Gallery::where('post_id', $id)->delete();

        $post = Post::findOrFail($id);
        $post->delete();

       return 'deleted';
    }

    public function deleteComment($id){
        $comment = Comment::findOrFail($id);
        $comment->delete();
        return 'deleted';
    }

    public function deleteGallery($id){
        $gallery = Gallery::findOrFail($id);
        $gallery->delete();
        return 'deleted';
    }

    public function deleteMessage($id){
        $message = Message::findOrFail($id);
        $message->delete();
        return 'deleted';
    }

    public function deleteClasses($id){
        $classes = Classes::findOrFail($id);
        $classes->delete();
        return 'deleted';
    }

    public function deleteSubject($id){
        $subject = Subject::findOrFail($id);
        $subject->delete();
        return 'deleted';
    }

    public function deleteLevel($id){
        $level = ClassLevel::findOrFail($id);
        $level->delete();
        return 'deleted';
    }

    public function deleteStatisticClass($id){
        $student = Statistic::findOrFail($id);
        $student->delete();

        return "deleted";
    }

    public function deleteHeadInfo($id){
        $head_info = HeadMaster::findOrFail($id);
        $head_info->delete();

        return "deleted";
    }

    public function deleteBookInfo($id){
        $book_info = Book::findOrFail($id);
        $book_info->delete();

        return "deleted";
    }

    public function deleteAdvert($id){
         $advert = Advert::findOrFail($id);
         $advert->delete();
        return "deleted";
    }



    //=========================================
    //  View Details
    //=========================================
    public function viewSchool($id){
         $schools = School::getSchoolInfo($id);
         return json_encode($schools);
    }

    public function viewStudents($id){
        $students = School::getSchoolClass($id);
        return json_encode($students);
    }

    public function viewSchoolHead($id){
        $head_master = School::getSchoolHead($id);
        return json_encode($head_master);
    }

    public function viewSchoolRegister($id){
        $register = School::getSchoolRegister($id);
        return json_encode($register);
    }

    public function viewSchoolClassLevels($id){
        $classLevels = School::getSchoolLevel($id);
        return json_encode($classLevels);
    }

    public function viewSchoolBook($id){
        $books = School::getSchoolBook($id);
        return json_encode($books);
    }

    public function viewAllSchools(){
        $schools = School::where('deleted_at',null)
                   ->orderBy('school_name', 'ASC')
                   ->get();
        return json_encode($schools);
    }

    public function viewAllSchoolRegion($id){
        $schools = School::where('region_id', $id)
                   ->where('deleted_at',null)
                   ->orderBy('school_name', 'ASC')
                   ->get();
        return json_encode($schools);
    }

    public function viewAllSchoolDistrict($reg, $dis){
        $schools = School::where('region_id', $reg)
                    ->where('district_id', $dis)
                    ->where('deleted_at',null)
                    ->orderBy('school_name', 'ASC')
                    ->get();
        return json_encode($schools);
    }

    public function viewAllSchoolZone($reg, $dis, $zone){
        $schools = School::where('region_id', $reg)
                    ->where('district_id', $dis)
                    ->where('zone_id', $zone)
                    ->where('deleted_at',null)
                    ->orderBy('school_name', 'ASC')
                    ->get();
        return json_encode($schools);
    }

    public function viewAllSchoolGroup($id){
        $schools = School::where('status', $id)
                    ->where('deleted_at',null)
                    ->orderBy('school_name', 'ASC')
                    ->get();
        return json_encode($schools);
    }

    public function getSchoolStatistic(){
        $school = DB::table('schools')
                  ->leftJoin('statistics','schools.id','=','statistics.school_id')
                  ->select('schools.id','schools.school_name','schools.town','schools.suburb',
                      DB::raw('sum(statistics.enroll_boys) as total_boys'),  DB::raw('sum(statistics.enroll_girls) as total_girls'),
                      DB::raw('sum(statistics.male_trained) as total_m_trained'), DB::raw('sum(statistics.male_untrained) as total_m_untrained'),
                      DB::raw('sum(statistics.female_trained) as total_f_trained'), DB::raw('sum(statistics.female_trained) as total_f_untrained'))
                  ->groupBy('schools.id','schools.school_name','schools.town','schools.suburb')
                  ->where('schools.deleted_at', null)
                  ->orderBy('schools.school_name','ASC')
                  ->get();

        return json_encode($school);
    }

    public function publishAdvert($id){
        $advert = Advert::findOrFail($id);
        $advert->status = '1';
        $advert->update();
        return "update";
    }

    public function declineAdvert($id){
        $advert = Advert::findOrFail($id);
        $advert->status = '0';
        $advert->update();
        return "update";
    }

    public function activeCarousel($id){
        $carousel = Carousel::findOrFail($id);
        $carousel->status = '1';
        $carousel->update();

        return "updated";
    }

    public function hideCarousel($id){
        $carousel = Carousel::findOrFail($id);
        $carousel->status = '0';
        $carousel->update();

        return "updated";
    }

}
