<?php

namespace App\Http\Controllers;

use App\AboutUs;
use App\Advert;
use App\Book;
use App\Carousel;
use App\Classes;
use App\ClassLevel;
use App\Comment;
use App\District;
use App\Gallery;
use App\HeadMaster;
use App\Message;
use App\Post;
use App\Region;
use App\Report;
use App\School;
use App\SchoolClass;
use App\SchoolRegister;
use App\Statistic;
use App\Subject;
use App\User;
use App\Zonal;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Auth;
use Intervention\Image\Facades\Image;

class PageController extends Controller
{
    //=================================
    // Pages Functions
    //=================================

    public function index(){
        return view('Page.index');
    }

    public function about(){
        return view('Page.about');
    }

    public function gallery(){
        return view('Page.gallery');
    }

    public function executives(){
        return view('Page.contact');
    }

    public function report(){
        return view('Page.report');
    }

    public function register(){
        return view('Page.register');
    }

    public function schoolInfo(){
        return view('Page.school');
    }

    public function viewPost($id){
        $post = Post::where('id', $id)
               ->where('deleted_at',null)
               ->first();

        $comment = Comment::where('post_id',$id)
                   ->orderBy('created_at','desc')
                   ->where('deleted_at', null)
                   ->get();

        $gallery = Gallery::where('post_id', $id)
                  ->where('deleted_at', null)
                  ->get();

        return view('Page.view', compact('post','comment','gallery'));
    }


    //===================================
    // Get Functions
    //===================================
    public function userLogin(Request $request){
        $validate = Validator::make($request->all(), [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_admin'=> 'Admin'],
            $request->remember)){
            return 'Admin';
        } else {
            return response()->json(['error' => 'User Credentials does not exist'],422);
        }
    }

    public function logOut(){
        Auth::logout();
        return redirect()->route('login');
    }

    public function getExecutives(){
        $user = User::where('deleted_at', null)
            ->where('user_type','Executive')
            ->orderBy('position','asc')
            ->get();
        return json_encode($user);
    }

    public function getAllPost(){
        $post = DB::table('posts')
                ->leftJoin('comments','posts.id','=','comments.post_id')
                ->select('posts.id','posts.title','posts.body','posts.image','posts.posted_on', DB::raw('count(comments.post_id) as totalcomment'))
                ->groupBy('posts.id','posts.title','posts.body','posts.image','posts.posted_on','posts.created_at')
                ->where('posts.deleted_at', null)
                ->orderBy('posts.created_at','desc')
                ->get();

        return json_encode($post);
    }

    public function getAllGallery(){
        $gallery = Gallery::leftJoin('posts','galleries.post_id','=','posts.id')
                   ->select('galleries.*','posts.title')
                   ->where('galleries.deleted_at', null)
                   ->where('posts.deleted_at', null)
                   ->orderBy('posts.created_at','desc')
                   ->get();

        return json_encode($gallery);
    }

    public function getLatestPost(){
        $post = Post::where('deleted_at',null)
            ->orderBy('created_at','desc')
            ->get();

        return json_encode($post);
    }

    public function getPost($id){
        $post = Post::where('id', $id)
            ->where('deleted_at',null)
            ->first();

        return json_encode($post);
    }

    public function getComment($id){
        $comment = Comment::where('post_id', $id)
            ->orderBy('created_at','desc')
            ->where('deleted_at', null)
            ->get();

        return json_encode($comment);
    }

    public function getGallery($id){
        $gallery = Gallery::where('post_id', $id)
            ->where('deleted_at', null)
            ->get();

        return json_decode($gallery);
    }

    public function getClasses(){
        $classes = Classes::where('deleted_at', null)
            ->orderBy('name','ASC')
            ->get();
        return json_encode($classes);
    }

    public function getSubject(){
        $subject = Subject::where('deleted_at', null)
            ->orderBy('subject','ASC')
            ->get();

        return json_encode($subject);
    }

    public function getLevel(){
        $level = ClassLevel::where('deleted_at', null)
            ->orderBy('name','ASC')
            ->get();

        return json_encode($level);
    }

    public function getRegion(){
        $region = Region::where('deleted_at',null)
                  ->orderBy('name','ASC')
                  ->get();

        return json_encode($region);
    }

    public function getDistrict($id){
        $district = District::where('deleted_at',null)
                    ->where('region_id', $id)
                    ->orderBy('name','ASC')
                    ->get();

        return json_encode($district);
    }

    public function getZone($id){
        $zonal = Zonal::where('deleted_at',null)
                ->where('district_id', $id)
                ->orderBy('name','ASC')
                ->get();

        return json_encode($zonal);
    }

    public function getAbout(){
        $about = AboutUs::where('deleted_at', null)->get();
        return json_encode($about);
    }

    public function getCarousel(){
        $carousel = Carousel::where('status','=','1')
                    ->orderBy('created_at','desc')
                    ->latest()
                    ->take(7)
                    ->get();

        return json_encode($carousel);
    }

    public function getAllReport(){
        $report = Report::where('deleted_at', null)
                 ->orderBy('created_at', 'desc')
                 ->get();

        return json_encode($report);
    }

    public function getAdvert(){
        $advert = Advert::where('status', '1')
                ->orderBy('created_at','desc')
                ->latest()
                ->take(5)
                ->get();

        return json_encode($advert);
    }


    //====================================
    //  Post Functions
    //====================================

    public function sendMessage(Request $request){
        $validate = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|string',
            'contact' => 'required|string',
            'subject' => 'required',
            'message' => 'required|min:6',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        try{

            $message = new Message();
            $message->name = $request['name'];
            $message->email = $request['email'];
            $message->contact = $request['contact'];
            $message->subject = $request['subject'];
            $message->message = $request['message'];
            $message->save();

        }catch (\Exception $e){
            return response()->json(['error' => $e],402);
        }

        return "saved";
    }

    public function sendComment(Request $request){
        $validate = Validator::make($request->all(), [
            'post_id' => 'required',
            'comment' => 'required|string',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        try {

            $comment = new Comment();
            $comment->post_id = $request['post_id'];
            $comment->comment = $request['comment'];
            $comment->save();

        } catch (\Exception $e) {
            return response()->json(['error' => $e],402);
        }

        return "saved";
    }

    public function saveUsers(Request $request){
        $validate = Validator::make($request->all(), [
            'firstName' => 'required|string|min:2',
            'otherName' => 'required|string|min:2',
            'email' => 'email|unique:users,email|nullable',
            'contact' => 'required',
            'position' => 'required|string'
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        //generate staff #
        $randomNumber = 'GNAPSASR'.str_pad(mt_rand(1, 9999), 4, '0', STR_PAD_LEFT).date('y');


        //checking if staff # already exist in the school
        $foundStaff = DB::table('users')
                    ->where('userID', '=', $randomNumber)
                    ->first();

        // if staff # already exists in the database, create a new one
        while($foundStaff != null) {
            $randomNumber = 'GNAPSASR'.str_pad(mt_rand(1, 9999), 4, '0', STR_PAD_LEFT).date('y');
        }


        try{

            $user = new User();
            $user->userID = $randomNumber;
            $user->first_name = $request['firstName'];
            $user->other_name = $request['otherName'];
            $user->contact = $request['contact'];
            $user->position = $request['position'];
            $user->email = $request['email'];
            $user->is_admin = 'Member';
            $user->user_type = 'Member';
            $user->password = bcrypt($request['firstName']);

            if($request->hasFile('image')){
                $picture  = $request->file('image');
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->save( public_path('images/users/' . $filename));
                $user->image = $filename;
            } else {
                $user->image = 'avatar.png';
            }

            $user->save();

            return $user->userID;

        } catch (\Exception $e) {
            return response()->json(['error' => $e],402);
        }
    }

    public function saveSchool(Request $request){
        $validate = Validator::make($request->all(), [
            'user_id' => 'required|string',
            'school_name' => 'required|string|min:2',
            'school_contact' => 'required|string',
            'school_address' => 'required|string',
            'school_facility' => 'required|string',
            'established_date' => 'required|string',
            'region' => 'required|string',
            'district' => 'required|string',
            'town' => 'required|string',
            'suburb' => 'required|string',
            'levels' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        $userID = DB::table('users')
                  ->where('userID', '=', $request['user_id'])
                  ->first();

        //generate staff #
        $randomNumber = 'GNAPSASR'.str_pad(mt_rand(1, 9999), 4, '0', STR_PAD_LEFT).date('y').'S';


        //checking if staff # already exist in the school
        $foundStaff = DB::table('schools')
                    ->where('schoolID', '=', $randomNumber)
                    ->first();

        // if staff # already exists in the database, create a new one
        while($foundStaff != null) {
            $randomNumber = 'GNAPSASR'.str_pad(mt_rand(1, 9999), 4, '0', STR_PAD_LEFT).date('y').'S';
        }


        try{

            $school = new School();
            $school->schoolID = $randomNumber;
            $school->school_name = $request['school_name'];
            $school->email = $request['school_email'];
            $school->contact = $request['school_contact'];
            $school->address = $request['school_address'];
            $school->town = $request['town'];
            $school->suburb = $request['suburb'];
            $school->facilities = $request['school_facility'];
            $school->website_url = $request['school_website'];
            $school->date_established = $request['established_date'];
            $school->region_id = $request['region'];
            $school->district_id = $request['district'];
            $school->zone_id = $request['zone'];
            $school->user_id = $userID->id;


            if($request->hasFile('logo')){
                $picture  = $request->file('logo');
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->save( public_path('images/schools/' . $filename));
                $school->logo = $filename;
            } else {
                $school->image = 'avatar.png';
            }

            $school->save();

            for ($i = 0; $i < count($request['levels']); $i++) {

                $dataExist = DB::table('school_classes')
                            ->select('level_id','school_id')
                            ->where('level_id', $request['levels'][$i])
                            ->where('school_id', $school->id)
                            ->first();

                $data = ClassLevel::where('id',$request['levels'][$i])
                        ->where('deleted_at', null)
                        ->first();

                if ($dataExist) {
                    return response()->json(['error' => $data->name . ' has already been assigned to school'],
                        401);
                }

                $school_class = new SchoolClass();
                $school_class->level_id = $request['levels'][$i];
                $school_class->school_id = $school->id;
                $school_class->save();
            }

            return $school->schoolID;

        }catch (\Exception $e){
            return response()->json(['error' => $e],402);
        }
    }

    public function saveClasses(Request $request){
        $validate = Validator::make($request->all(), [
            'school_id' => 'required',
            'classID' => 'required',
            'classroom' => 'required',
            'boys' => 'required',
            'girls' => 'required',
            'male_trained' => 'required',
            'male_untrained' => 'required',
            'female_trained' => 'required',
            'female_untrained' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        $schoolID = DB::table('schools')
                    ->where('schoolID', '=', strtoupper($request['school_id']))
                    ->first();

        try {

            for ($i = 0; $i < count($request['classID']); $i++) {

                $dataExist = DB::table('statistics')
                            ->select('class_id','school_id')
                            ->where('class_id', $request['classID'][$i])
                            ->where('school_id', $schoolID->id)
                            ->first();

                $data = Classes::where('id',$request['classID'][$i])
                        ->where('deleted_at', null)
                        ->first();

                if ($dataExist) {
                    return response()->json(['error' => $data->name . ' statistics has already been added'],
                        401);
                }

                $statistic = new Statistic();
                $statistic->class_id = $request['classID'][$i];
                $statistic->school_id = $schoolID->id;
                $statistic->classrooms = $request['classroom'][$i];
                $statistic->enroll_boys = $request['boys'][$i];
                $statistic->enroll_girls = $request['girls'][$i];
                $statistic->male_trained = $request['male_trained'][$i];
                $statistic->male_untrained = $request['male_untrained'][$i];
                $statistic->female_trained = $request['female_trained'][$i];
                $statistic->female_untrained = $request['female_untrained'][$i];
                $statistic->save();
            }

            return 'saved';

        } catch (\Exception $e) {
            return response()->json(['error' => $e],402);
        }
    }

    public function saveSchoolHead(Request $request){
        $validate = Validator::make($request->all(), [
            'school_id' => 'required',
            'head_name' => 'required',
            'head_contact' => 'required',
            'class_form' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        $schoolID = DB::table('schools')
                    ->where('schoolID', '=', strtoupper($request['school_id']))
                    ->first();

        try {

            for ($i = 0; $i < count($request['head_name']); $i++) {

                $dataExist = DB::table('head_masters')
                            ->select('contact','school_id')
                            ->where('contact', $request['head_contact'][$i])
                            ->where('school_id', $schoolID->id)
                            ->first();

                if ($dataExist) {
                    return response()->json(['error' => $request['head_name'][$i].' '.$request['head_contact'][$i].' '.'has already been registered'],
                        401);
                }

                $head_master = new HeadMaster();
                $head_master->head_name = $request['head_name'][$i];
                $head_master->contact = $request['head_contact'][$i];
                $head_master->class_form = $request['class_form'][$i];
                $head_master->school_id = $schoolID->id;
                $head_master->save();
            }

            return 'saved';

        } catch (\Exception $e) {
            return response()->json(['error' => $e],402);
        }
    }

    public function saveSchoolTextBook(Request $request){
        $validate = Validator::make($request->all(), [
            'school_id' => 'required',
            'classID' => 'required',
            'subjectID' => 'required',
            'totalBooks' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        $schoolID = DB::table('schools')
                    ->where('schoolID', '=', strtoupper($request['school_id']))
                    ->first();

        try {

            for ($i = 0; $i < count($request['classID']); $i++) {

                $dataExist = DB::table('books')
                            ->select('class_id','school_id', 'subject_id')
                            ->where('class_id', $request['classID'][$i])
                            ->where('subject_id', $request['subjectID'][$i])
                            ->where('school_id', $schoolID->id)
                            ->first();

                $subject = Subject::where('id', $request['subjectID'][$i])->first();

                $class = Classes::where('id', $request['classID'][$i])->first();

                if ($dataExist) {
                    return response()->json(['error' => $subject->subject.' total textbooks has already been added to '
                        .$class->name],401);
                }

                $book = new Book();
                $book->class_id = $request['classID'][$i];
                $book->subject_id = $request['subjectID'][$i];
                $book->number = $request['totalBooks'][$i];
                $book->school_id = $schoolID->id;
                $book->save();
            }

            return 'saved';

        } catch (\Exception $e) {
            return response()->json(['error' => $e],402);
        }
    }

    public function saveSchoolRegister(Request $request){
        $validate = Validator::make($request->all(), [
            'school_id' => 'required',
            'reg_min' => 'required',
            'reg_gen' => 'required',
            'reg_gnaps' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => $validate->errors()],422);
        }

        $schoolID = DB::table('schools')
                    ->where('schoolID', '=', strtoupper($request['school_id']))
                    ->first();

        try {

            $dataExist = DB::table('school_registers')
                        ->select('school_id')
                        ->where('school_id', $schoolID->id)
                        ->first();

            if ($dataExist) {
                return response()->json(['error' => 'School registration records has already been added'],401);
            }

            $register = new SchoolRegister();
            $register->reg_with_min_eduction = $request['reg_min'];
            $register->reg_min_number = $request['min_number'];
            $register->registerar_general = $request['reg_gen'];
            $register->reg_gen_number = $request['gen_number'];
            $register->reg_with_gnaps = $request['reg_gnaps'];
            $register->gnaps_number = $request['gnaps_number'];
            $register->school_id = $schoolID->id;
            $register->save();

            return 'saved';

        } catch (\Exception $e) {
            return response()->json(['error' => $e],402);
        }
    }



    //=====================================================
    //  View School Record Functions
    //=====================================================

    public function getSchoolID(Request $request){
        $validate = Validator::make($request->all(), [
            'school_id' => 'required|string',
        ]);

        if ($validate->fails()) {
            return response()->json(['error' => 'Please provide your school ID'],422);
        }

        try {

            $school_id = strtoupper($request['school_id']);
            $schoolID = School::where('schoolID', $school_id)->where('deleted_at', null)->value('id');

            if($schoolID <= 0){
                return response()->json(['error' => 'No records for provided school id, check and provide the collect school ID'], 422);
            }

            return $schoolID;

        } catch (\Exception $e) {
           return response()->json(['error' => 'Provided school ID was not found in records'], 422);
        }
    }

    public function viewSchool($id){
        $schools = School::getSchoolInfo($id);
        return json_encode($schools);
    }

    public function viewStudents($id){
        $students = School::getSchoolClass($id);
        return json_encode($students);
    }

    public function viewSchoolHead($id){
        $head_master = School::getSchoolHead($id);
        return json_encode($head_master);
    }

    public function viewSchoolRegister($id){
        $register = School::getSchoolRegister($id);
        return json_encode($register);
    }

    public function viewSchoolClassLevels($id){
        $classLevels = School::getSchoolLevel($id);
        return json_encode($classLevels);
    }

    public function viewSchoolBook($id){
        $books = School::getSchoolBook($id);
        return json_encode($books);
    }


}




























