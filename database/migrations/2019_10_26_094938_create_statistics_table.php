<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('school_id')->nullable();
            $table->unsignedInteger('class_id')->nullable();
            $table->integer('classrooms');
            $table->integer('enroll_boys')->nullable();
            $table->integer('enroll_girls')->nullable();
            $table->integer('male_trained')->nullable();
            $table->integer('male_untrained')->nullable();
            $table->integer('female_trained')->nullable();
            $table->integer('female_untrained')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('class_id')
                    ->references('id')
                    ->on('classes')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('school_id')
                    ->references('id')
                    ->on('schools')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statistics');
    }
}
