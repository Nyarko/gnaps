<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_classes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('level_id')->nullable();
            $table->unsignedInteger('school_id')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('level_id')
                    ->references('id')
                    ->on('class_levels')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('school_id')
                    ->references('id')
                    ->on('schools')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_classes');
    }
}
