<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->increments('id');
            $table->string('schoolID');
            $table->string('school_name');
            $table->string('email')->nullable();
            $table->string('contact')->nullable();
            $table->string('address')->nullable();
            $table->string('town')->nullable();
            $table->string('suburb')->nullable();
            $table->enum('facilities', ['Day','Boarding','Day & Boarding']);
            $table->string('website_url')->nullable();
            $table->date('date_established')->nullable();
            $table->string('status')->default(0);
            $table->string('logo')->nullable();
            $table->integer('region_id')->unsigned()->nullable();
            $table->integer('district_id')->unsigned()->nullable();
            $table->integer('zone_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('region_id')
                    ->references('id')
                    ->on('regions')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('district_id')
                    ->references('id')
                    ->on('districts')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('zone_id')
                    ->references('id')
                    ->on('zonals')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
