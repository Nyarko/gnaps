<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_registers', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('reg_with_min_eduction', ['Yes','No']);
            $table->string('reg_min_number')->nullable();
            $table->enum('registerar_general', ['Yes','No']);
            $table->string('reg_gen_number')->nullable();
            $table->enum('reg_with_gnaps', ['Yes','No']);
            $table->string('gnaps_number')->nullable();
            $table->integer('school_id')->unsigned()->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('school_id')
                    ->references('id')
                    ->on('schools')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_registers');
    }
}
