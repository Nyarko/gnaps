<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zonals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('district_id')->unsigned()->nullable();
            $table->string('name');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('district_id')
                    ->references('id')
                    ->on('districts')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zonals');
    }
}
