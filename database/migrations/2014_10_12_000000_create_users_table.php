<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('userID');
            $table->string('first_name');
            $table->string('other_name');
            $table->string('contact');
            $table->string('position')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('image');
            $table->enum('is_admin', ['Admin','Member']);
            $table->enum('user_type', ['Executive','Member']);
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
