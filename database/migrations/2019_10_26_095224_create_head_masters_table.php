<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeadMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('head_masters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('head_name');
            $table->string('contact')->nullable();
            $table->string('class_form')->nullable();
            $table->unsignedInteger('school_id')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('school_id')
                    ->references('id')
                    ->on('schools')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('head_masters');
    }
}
