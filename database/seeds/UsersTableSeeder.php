<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'userID' => 'GNAPSASR123219',
                'first_name' => 'Super',
                'other_name' => 'Admin',
                'contact' => '054-4474-706',
                'position' => 'President',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('@Admin2019'),
                'image' => 'avatar.png',
                'is_admin' => 'Admin',
                'user_type' => 'Executive',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'userID' => 'GNAPSASR223219',
                'first_name' => 'Isaac',
                'other_name' => 'Nyarko',
                'contact' => '054-4474-706',
                'position' => 'Developer',
                'email' => 'nyarko@gmail.com',
                'password' => bcrypt('@Nyarko2018'),
                'image' => 'avatar.png',
                'is_admin' => 'Admin',
                'user_type' => 'Executive',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'userID' => 'GNAPSASR323219',
                'first_name' => 'Stephen',
                'other_name' => 'Ofori',
                'contact' => '054-4474-706',
                'position' => 'Manager',
                'email' => 'stephen@gmail.com',
                'password' => bcrypt('@Stephen2019'),
                'image' => 'avatar.png',
                'is_admin' => 'Admin',
                'user_type' => 'Executive',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],

        ]);
    }
}
