window.Vue = require('vue');

require('./MyFunction/bootstrap');
require('./MyFunction/function');

import Swal from 'sweetalert2';
import VueTelInput from 'vue-tel-input';
import VueHtmlToPaper from 'vue-html-to-paper';


//global registration
window.Swal = Swal;

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
});

const options = {
    name: '_blank',
    specs: [
        'fullscreen=yes',
        'titlebar=yes',
        'scrollbars=yes',
        'width=1200px',
        'height=800px',
    ],
    styles: [
        'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
        'https://unpkg.com/kidlat-css/css/kidlat.css'
    ]
};


//global registration
window.Toast = Toast;
window.Fire = new Vue();
Vue.use(VueTelInput);
Vue.use(VueHtmlToPaper, options);


// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
Vue.component('example-component', require('./components/ExampleComponent.vue').default);




// Admins Component
Vue.component('executive-component', require('./components/Admin/Executive').default);
Vue.component('dashboard-component', require('./components/Admin/Index').default);
Vue.component('region-component', require('./components/Admin/Region').default);
Vue.component('district-component', require('./components/Admin/District').default);
Vue.component('zonal-component', require('./components/Admin/Zonal').default);
Vue.component('post-component', require('./components/Admin/Post').default);
Vue.component('message-component', require('./components/Admin/Message').default);
Vue.component('profile-component', require('./components/Admin/Profile').default);
Vue.component('classes-component', require('./components/Admin/Classes').default);
Vue.component('level-component', require('./components/Admin/Levels').default);
Vue.component('subject-component', require('./components/Admin/Subject').default);
Vue.component('about-component', require('./components/Admin/AboutUs').default);
Vue.component('carousel-component', require('./components/Admin/Carousel').default);
Vue.component('school-owners-component', require('./components/Admin/SchoolOwners').default);
Vue.component('all-school-component', require('./components/Admin/AllSchools').default);
Vue.component('register-school-component', require('./components/Admin/RegisterSchool').default);
Vue.component('report-component', require('./components/Admin/Report').default);
Vue.component('view-school-component', require('./components/Admin/SchoolDetails').default);
Vue.component('view-gen-report-component', require('./components/Admin/GenerateReport').default);
Vue.component('advert-component', require('./components/Admin/Advert').default);


// Pages Component
Vue.component('login-component', require('./components/Page/Login').default);
Vue.component('contact-component', require('./components/Page/Executive').default);
Vue.component('page-post-component', require('./components/Page/AllPosts').default);
Vue.component('view-post-component', require('./components/Page/ViewPost').default);
Vue.component('page-message-component', require('./components/Page/Message').default);
Vue.component('page-gallery-component', require('./components/Page/Gallery').default);
Vue.component('page-carousel-component', require('./components/Page/Carousel').default);
Vue.component('page-register-component', require('./components/Page/Register').default);
Vue.component('page-about-component', require('./components/Page/AboutUS').default);
Vue.component('page-report-component', require('./components/Page/Report').default);
Vue.component('page-advert-component', require('./components/Page/Advert').default);
Vue.component('school-info-component', require('./components/Page/SchoolInfo').default);



const app = new Vue({
    el: '#app',
});
