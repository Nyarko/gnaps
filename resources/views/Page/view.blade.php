@extends('Layout.PageLayout')

@section('title', 'View Post')

@section('section')
    <div id="team" class="our-team-area area-padding">
        <view-post-component
        post="{{ $post  }}"
        comment="{{ $comment }}"
        gallery="{{ $gallery }}"></view-post-component>
    </div>
@endsection