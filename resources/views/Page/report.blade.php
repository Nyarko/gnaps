@extends('Layout.PageLayout')

@section('title', 'Annual Report')

@section('section')

    <div class="faq-area area-padding">

        <page-report-component></page-report-component>

    </div>
    
@endsection