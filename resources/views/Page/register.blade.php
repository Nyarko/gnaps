@extends('Layout.PageLayout')

@section('title', 'Register')

@section('section')

    <div class="faq-area area-padding">
        <page-register-component></page-register-component>
    </div>
    
@endsection