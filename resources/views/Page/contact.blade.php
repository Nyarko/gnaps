@extends('Layout.PageLayout')

@section('title', 'Executives')

@section('section')
    <div id="team" class="our-team-area area-padding">
        <contact-component></contact-component>
    </div>
@endsection