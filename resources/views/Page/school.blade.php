@extends('Layout.PageLayout')

@section('title', 'School Information')

@section('section')

    <div class="faq-area area-padding">
        <school-info-component></school-info-component>
    </div>
    
@endsection