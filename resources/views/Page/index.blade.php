@extends('Layout.PageLayout')

@section('title', 'Home')

@section('section')

    @include('Partials.page._carousel')

    <!-- Start Blog Area -->
    <div id="blog" class="blog-area">
        <div class="blog-inner area-padding">
            <div class="blog-overly"></div>
            <page-post-component></page-post-component>
        </div>
    </div>
    <!-- End Blog -->

@endsection