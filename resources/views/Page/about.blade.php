@extends('Layout.PageLayout')

@section('title', 'About US')

@section('section')
    <div id="about" class="about-area area-padding">

       <page-about-component></page-about-component>

    </div>
@endsection