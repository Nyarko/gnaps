@extends('Layout.AdminLayout')
@section('title', 'School Details')
@section('section')
    <view-school-component id="{{ $id }}"></view-school-component>
@endsection