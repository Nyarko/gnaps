@extends('Layout.AdminLayout')
@section('title', 'Dashboard')
@section('section')
   <dashboard-component
   allschool="{{ $allschool }}"
   gnapsmenber="{{ $schoolGnaps }}"
   gnapsnonmember="{{ $schoolNonGnaps }}"
   totalusers="{{ $allusers }}"
   totalexecutive="{{ $allexecutive }}"
   totalmember="{{ $allmembers }}"
   totalregion="{{ $allregions }}"
   totaldistrict="{{ $alldistrict }}"
   totalzone="{{ $allzone }}"
   totalpost="{{ $allpost }}"
   totalcomment="{{ $allcomment }}"
   totalmessage="{{ $allmessage }}"
   totalgallery="{{ $allgallery }}"
   totalstudentgirl="{{ $allgirlstudent }}"
   totalstudentboys="{{ $allboystudent }}"
   totalstudents="{{ $allstudents }}"
   totalmaletrained="{{ $alltrainedmale }}"
   totalmaleuntrained="{{ $allustrainedmale }}"
   totalfemaletrained="{{ $alltrainedfemale }}"
   totalfemaleuntrained="{{ $alluntrainedfemale }}"
   totalteachers="{{ $allteachers  }}"></dashboard-component>
@endsection