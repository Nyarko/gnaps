<header>
    <!-- header-area start -->
    <div id="sticker" class="header-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-sm-12">

                    <!-- Navigation -->
                    <nav class="navbar navbar-default">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <!-- Brand -->
                            <a class="navbar-brand page-scroll sticky-logo" href="{{ route('page.index') }}">
                                <h1 style="margin-top:-30px; margin-left: -60px">
                                    <img src="{{ asset('/images/newLogo.png') }}" alt="" height="40" width="360">
                                </h1>
                            </a>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse main-menu">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="{{ strtolower(Request::segment(1)) == ''?'active':'' }}">
                                    <a class="page-scroll" href="{{ route('page.index') }}">
                                        Home
                                    </a>
                                </li>
                                <li class="{{ strtolower(Request::segment(1)) == 'about'?'active':''}}">
                                    <a class="page-scroll" href="{{ route('page.about') }}">
                                         About
                                    </a>
                                </li>
                                <li class="{{ strtolower(Request::segment(1)) == 'gallery'?'active':''}}">
                                    <a class="page-scroll" href="{{ route('page.gallery') }}">
                                         Gallery
                                    </a>
                                </li>

                                <li class="{{ strtolower(Request::segment(1)) == 'executive'?'active':''}}">
                                    <a class="page-scroll" href="{{ route('page.executive') }}">
                                       Executives
                                    </a>
                                </li>
                                <li class="{{ strtolower(Request::segment(1)) == 'report'?'active':''}}">
                                    <a class="page-scroll" href="{{ route('page.report') }}">
                                         Report
                                    </a>
                                </li>
                                <li class="{{ strtolower(Request::segment(1)) == 'register'?'active':''}}">
                                    <a class="page-scroll" href="{{ route('page.register') }}">
                                        Register
                                    </a>
                                </li>
                                <li>
                                    <a class="page-scroll" data-toggle="modal" data-target="#userLogin" href="#">
                                         Login
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- navbar-collapse -->
                    </nav>
                    <!-- END: Navigation -->
                </div>
            </div>
        </div>
    </div>
    <!-- header-area end -->
</header>

<style>
    .active a {
        font-weight: bolder;
    }
</style>
