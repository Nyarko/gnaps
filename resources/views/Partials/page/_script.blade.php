<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

<!-- JavaScript Libraries -->
<script src="{{ asset('/js/app.js') }}"></script>
<script src="{{ asset('/page/lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('/page/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/page/lib/owlcarousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('/page/lib/venobox/venobox.min.js') }}"></script>
<script src="{{ asset('/page/lib/knob/jquery.knob.js') }}"></script>
<script src="{{ asset('/page/lib/wow/wow.min.js') }}"></script>
<script src="{{ asset('/page/lib/parallax/parallax.js') }}"></script>
<script src="{{ asset('/page/lib/easing/easing.min.js') }}"></script>
<script src="{{ asset('/page/lib/nivo-slider/js/jquery.nivo.slider.js') }}" type="text/javascript"></script>
<script src="{{ asset('/page/lib/appear/jquery.appear.js') }}"></script>
<script src="{{ asset('/page/lib/isotope/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('/page/js/main.js') }}"></script>

<script language="javascript" type="text/javascript">
    $(window).load(function() {
        $('#loading').hide();
    });
</script>