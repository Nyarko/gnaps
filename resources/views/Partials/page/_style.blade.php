<meta charset="utf-8">
<title>GNAPS | @yield('title')</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<meta content="" name="keywords">
<meta content="" name="description">

<link rel="icon" href="{{ asset('/images/logo.png') }}">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">

<!-- Bootstrap CSS File -->
<link href="{{ asset('/page/lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

<!-- Libraries CSS Files -->
<link href="{{ asset('/page/lib/nivo-slider/css/nivo-slider.css') }}" rel="stylesheet">
<link href="{{ asset('/page/lib/owlcarousel/owl.carousel.css') }}" rel="stylesheet">
<link href="{{ asset('/page/lib/owlcarousel/owl.transitions.css') }}" rel="stylesheet">
<link href="{{ asset('/page/lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('/page/lib/animate/animate.min.css') }}" rel="stylesheet">
<link href="{{ asset('/page/lib/venobox/venobox.css') }}" rel="stylesheet">

<!-- Nivo Slider Theme -->
<link href="{{ asset('/page/css/nivo-slider-theme.css') }}" rel="stylesheet">

<!-- Main Stylesheet File -->
<link href="{{ asset('/page/css/style.css') }}" rel="stylesheet">

<!-- Responsive Stylesheet File -->
<link href="{{ asset('/page/css/responsive.css') }}" rel="stylesheet">
<link href="{{ asset('/css/custom.css') }}" rel="stylesheet">