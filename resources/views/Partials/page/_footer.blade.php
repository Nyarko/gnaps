
<div class="testimonials-area">
    <div class="testi-inner area-padding">
        <div class="testi-overly"></div>

        <page-advert-component></page-advert-component>

    </div>
</div>


<div id="contact" class="contact-area">
    <hr>
    <div class="contact-inner">
        <div class="contact-overly"></div>

        <page-message-component></page-message-component>

    </div>
</div>


<footer>
    <div class="footer-area-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-content">
                        <div class="footer-head">
                            <div class="footer-icons">
                                <ul>
                                    <li >
                                        <a href="http://www.facebook.com/gnapsashantiregion" target="_blank" style="background-color: blue">
                                          <i class="fa fa-facebook" style="color: white"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://www.twitter.com/GnapsAshanti" target="_blank"
                                           style="background-color: #1DA1F2">
                                            <i class="fa fa-twitter" style="color:white"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>