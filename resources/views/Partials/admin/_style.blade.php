<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
<meta name="author" content="GeeksLabs">
<meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
<link rel="shortcut icon" href="{{ asset('/images/logo.png') }}">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">


<title>GNAPS | @yield('title')</title>


<link href="{{ asset('/admin/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('/admin/css/bootstrap-theme.css') }}" rel="stylesheet">
<link href="{{ asset('/admin/css/elegant-icons-style.css') }}" rel="stylesheet"/>
<link href="{{ asset('/page/lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('/admin/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('/admin/css/style-responsive.css') }}" rel="stylesheet" />
