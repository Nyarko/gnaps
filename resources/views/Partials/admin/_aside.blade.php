<!--sidebar start-->
<aside>
    <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
            <li class="{{ strtolower(Request::segment(2)) == 'index'?'active':''}}">
                <a class="" href="{{ route('admin.index') }}">
                    <i class="icon_house_alt" style="color: #ffb505"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="fa fa-location-arrow" style="color: #c0e3ff"></i>
                    <span>Location</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul class="sub" style="{{strtolower(Request::segment(2)) == 'loc'?'display:block':''}}">
                    <li class="{{ strtolower(Request::segment(3)) == 'region'?'active':''}}">
                        <a class="" href="{{ route('admin.region') }}">
                            <i class="fa fa-refresh" style="color: #0f2bff"></i>
                            <span>Regions</span>
                        </a>
                    </li>
                    <li class="{{ strtolower(Request::segment(3)) == 'district'?'active':''}}">
                        <a class="" href="{{ route('admin.district') }}">
                            <i class="fa fa-th" style="color: #ff0e33"></i>
                            <span>Districts</span>
                        </a>
                    </li>
                    <li class="{{ strtolower(Request::segment(3)) == 'zonal'?'active':''}}">
                        <a class="" href="{{ route('admin.zonal') }}">
                            <i class="fa fa-rss" style="color: #13ff62"></i>
                            <span>Zonals</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="{{ strtolower(Request::segment(2)) == 'executive'?'active':''}}">
                <a class="" href="{{ route('admin.executive') }}">
                    <i class="fa fa-user" style="color: #11fffe"></i>
                    <span>Executives</span>
                </a>
            </li>
            <li class="{{ strtolower(Request::segment(2)) == 'post'?'active':''}}">
                <a class="" href="{{ route('admin.post') }}">
                    <i class="fa fa-hand-pointer-o" style="color: #ffd6d8"></i>
                    <span>Posts</span>
                </a>
            </li>
            <li class="{{ strtolower(Request::segment(2)) == 'message'?'active':''}}">
                <a class="" href="{{ route('admin.message') }}">
                    <i class="fa fa-mail-reply" style="color: #5bffb8"></i>
                    <span>Messages</span>
                </a>
            </li>
            <li class="{{ strtolower(Request::segment(2)) == 'about'?'active':''}}">
                <a class="" href="{{ route('admin.aboutus') }}">
                    <i class="fa fa-history" style="color: #645fff"></i>
                    <span>About Gnaps</span>
                </a>
            </li>
            <li class="{{ strtolower(Request::segment(2)) == 'carousel'?'active':''}}">
                <a class="" href="{{ route('admin.carousel') }}">
                    <i class="fa fa-file-picture-o" style="color: #ebff07"></i>
                    <span>Carousel</span>
                </a>
            </li>

            <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="fa fa-graduation-cap" style="color: #ff117f"></i>
                    <span>Schools</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul class="sub" style="{{strtolower(Request::segment(2)) == 'sch'?'display:block':''}}">
                    <li class="{{ strtolower(Request::segment(3)) == 'classes'?'active':''}}">
                        <a class="" href="{{ route('admin.classes') }}">
                            <i class="fa fa-clone" style="color: #1dddff"></i>
                            <span>Classes</span>
                        </a>
                    </li>
                    <li class="{{ strtolower(Request::segment(3)) == 'subject'?'active':''}}">
                        <a class="" href="{{ route('admin.subject') }}">
                            <i class="fa fa-bookmark" style="color: #f2beff"></i>
                            <span>Subjects</span>
                        </a>
                    </li>
                    <li class="{{ strtolower(Request::segment(3)) == 'levels'?'active':''}}">
                        <a class="" href="{{ route('admin.level') }}">
                            <i class="fa fa-leaf" style="color: #0eff10"></i>
                            <span>Levels</span>
                        </a>
                    </li>
                    <li class="{{ strtolower(Request::segment(3)) == 'schools'?'active':''}}">
                        <a class="" href="{{ route('admin.school') }}">
                            <i class="fa fa-graduation-cap" style="color: #bc81ff"></i>
                            <span> All Schools</span>
                        </a>
                    </li>
                    <li class="{{ strtolower(Request::segment(3)) == 'add'?'active':''}}">
                        <a class="" href="{{ route('admin.school.add') }}">
                            <i class="fa fa-plus-circle" style="color: #1107ff"></i>
                            <span> Add School</span>
                        </a>
                    </li>
                    <li class="{{ strtolower(Request::segment(3)) == 'owners'?'active':''}}">
                        <a class="" href="{{ route('admin.owner') }}">
                            <i class="fa fa-user" style="color: #ffb4d2"></i>
                            <span>School Owners</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="fa fa-paperclip" style="color: #ffc38c"></i>
                    <span>Report</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul class="sub" style="{{strtolower(Request::segment(2)) == 'rep'?'display:block':''}}">
                    <li class="{{ strtolower(Request::segment(3)) == 'report'?'active':''}}">
                        <a class="" href="{{ route('admin.report') }}">
                            <i class="fa fa-file" style="color: #0fff72"></i>
                            <span>Annual Report</span>
                        </a>
                    </li>
                    <li class="{{ strtolower(Request::segment(3)) == 'gen_report'?'active':''}}">
                        <a class="" href="{{ route('admin.gen.report') }}">
                            <i class="fa fa-file-archive-o" style="color: #a70dff"></i>
                            <span>Generate Report</span>
                        </a>
                    </li>
                    <li class="{{ strtolower(Request::segment(3)) == 'advert'?'active':''}}">
                        <a class="" href="{{ route('admin.advert') }}">
                            <i class="fa fa-adn" style="color: #407fff"></i>
                            <span>Advert</span>
                        </a>
                    </li>
                </ul>
            </li>

        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->