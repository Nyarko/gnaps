<header class="header dark-bg">

    <div class="toggle-nav">
        <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom">
            <i class="icon_menu"></i>
        </div>
    </div>

    <!--logo start-->
    <a href="{{ route('admin.index') }}" class="logo">
        <img src="{{ asset('/images/logo.png') }}" alt="" width="30px" height="35px">
        <span class="lite"><b>GNAPS</b></span>
    </a>
    <!--logo end-->


    <div class="top-nav notification-row">

        <ul class="nav pull-right top-menu">


            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    @if(Auth::Check())
                        <span class="profile-ava">
                                <img alt="" src="{{ Auth::user()->image }}" width="30" height="30">
                        </span>
                        <span class="username">{{ Auth::user()->first_name }} </span>
                        <b class="caret"></b>
                    @endif
                </a>
                <ul class="dropdown-menu extended logout">
                    <div class="log-arrow-up"></div>

                    <li class="eborder-top">
                        <a href="#profile" data-toggle="modal">
                            <i class="icon_profile"></i> My Profile
                        </a>
                    </li>

                    <li>
                        <a href="#change_password" data-toggle="modal">
                            <i class="icon_key_alt"></i> Change Password
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('logout') }}">
                            <i class="icon_lock_alt"></i> Log Out
                        </a>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
</header>
<!--header end-->

<profile-component userinfo="{{ Auth::user() }}"></profile-component>