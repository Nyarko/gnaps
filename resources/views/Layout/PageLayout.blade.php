<!Doctype html>
<html lang="en">
    <head>
         @include('Partials.page._style')
    </head>
    <body data-spy="scroll" data-target="#navbar-example">

          @include('Partials.page._header')

          <div id="app">
              <login-component></login-component>
              @yield('section')
              @include('Partials.page._footer')
          </div>

          @include('Partials.page._script')
          @include('Partials.loader')

    </body>
</html>
