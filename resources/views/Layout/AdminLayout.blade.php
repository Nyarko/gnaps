<!DOCTYPE html>
<html lang="en">

<head>

    @include('Partials.admin._style')

    <script type="text/javascript">
        window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?>
    </script>

</head>

<body>

   <section id="container">

       <div id="app">

           @include('Partials.admin._header')
           @include('Partials.admin._aside')

           <section id="main-content">
              @yield('section')
           </section>

       </div>

   </section>

   @include('Partials.admin._script')

</body>

</html>
