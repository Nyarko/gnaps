<?php

//===============================
// Routes For Front end Goes Here
//===============================

Route::get('/login', 'PageController@index')->name('login');
Route::get('/logout', 'PageController@index')->name('logout');

Route::prefix('/')->group(function (){

    // page route
    Route::get('/', 'PageController@index')->name('page.index');
    Route::get('/about', 'PageController@about')->name('page.about');
    Route::get('/gallery', 'PageController@gallery')->name('page.gallery');
    Route::get('/executive', 'PageController@executives')->name('page.executive');
    Route::get('/report', 'PageController@report')->name('page.report');
    Route::get('page/register', 'PageController@register')->name('page.register');
    Route::get('/school/info', 'PageController@schoolInfo')->name('sch.info');

    // get records
    Route::get('/executive/users', 'PageController@getExecutives');
    Route::get('/all/posts', 'PageController@getAllPost');
    Route::get('/all/gallery', 'PageController@getAllGallery');
    Route::get('/latest/posts', 'PageController@getLatestPost');
    Route::get('/all/classes', 'PageController@getClasses');
    Route::get('/all/subjects', 'PageController@getSubject');
    Route::get('/all/levels', 'PageController@getLevel');
    Route::get('/all/region', 'PageController@getRegion');
    Route::get('/all/about', 'PageController@getAbout');
    Route::get('/all/carousel', 'PageController@getCarousel');
    Route::get('/all/report', 'PageController@getAllReport');
    Route::get('/get/advert', 'PageController@getAdvert');


    // get with id
    Route::get('/all/district/{id}', 'PageController@getDistrict');
    Route::get('/all/zonal/{id}', 'PageController@getZone');
    Route::get('/view/post/{id}', 'PageController@viewPost');
    Route::get('/single/post/{id}', 'PageController@getPost');
    Route::get('/single/comment/{id}', 'PageController@getComment');
    Route::get('/single/gallery/{id}', 'PageController@getGallery');


    //update records
    Route::post('/update/statistic', 'AdminController@updateStatisticClasses');
    Route::post('/update/head', 'AdminController@updateSchoolHead');
    Route::post('/update/school_register', 'AdminController@updateSchoolRegister');
    Route::post('/update/school_book', 'AdminController@updateSchoolTextBook');



    // post route
    Route::post('/user/login', 'PageController@userLogin');
    Route::post('/save/message', 'PageController@sendMessage');
    Route::post('/save/comment', 'PageController@sendComment');
    Route::post('/save/users', 'PageController@saveUsers');
    Route::post('/save/school', 'PageController@saveSchool');
    Route::post('/save/classes/statistics', 'PageController@saveClasses');
    Route::post('/save/heads/info', 'PageController@saveSchoolHead');
    Route::post('/save/school/textbooks', 'PageController@saveSchoolTextBook');
    Route::post('/save/school/register', 'PageController@saveSchoolRegister');
    Route::post('/save/school/id', 'PageController@getSchoolID');


    // view route using id
    Route::get('/view/school/info/{id}', 'PageController@viewSchool');
    Route::get('/view/school/student/{id}', 'PageController@viewStudents');
    Route::get('/view/school/head/{id}', 'PageController@viewSchoolHead');
    Route::get('/view/school/register/{id}', 'PageController@viewSchoolRegister');
    Route::get('/view/school/levels/{id}', 'PageController@viewSchoolClassLevels');
    Route::get('/view/school/books/{id}', 'PageController@viewSchoolBook');


});

Route::group(['middleware' => ['auth', 'Admin']], function(){

    Route::prefix('/admin')->group(function (){

        // page route
        Route::get('/index', 'AdminController@index')->name('admin.index');
        Route::get('/executive', 'AdminController@executive')->name('admin.executive');
        Route::get('/loc/region', 'AdminController@region')->name('admin.region');
        Route::get('/loc/district', 'AdminController@district')->name('admin.district');
        Route::get('/loc/zonal', 'AdminController@zonals')->name('admin.zonal');
        Route::get('/post', 'AdminController@post')->name('admin.post');
        Route::get('/message', 'AdminController@message')->name('admin.message');
        Route::get('/sch/classes', 'AdminController@classes')->name('admin.classes');
        Route::get('/sch/levels', 'AdminController@level')->name('admin.level');
        Route::get('/sch/subject', 'AdminController@subject')->name('admin.subject');
        Route::get('/sch/owners', 'AdminController@schoolOwner')->name('admin.owner');
        Route::get('/about', 'AdminController@aboutus')->name('admin.aboutus');
        Route::get('/carousel', 'AdminController@carousel')->name('admin.carousel');
        Route::get('/sch/schools', 'AdminController@allSchool')->name('admin.school');
        Route::get('/sch/add', 'AdminController@addSchool')->name('admin.school.add');
        Route::get('/rep/report', 'AdminController@addReport')->name('admin.report');
        Route::get('/rep/gen_report', 'AdminController@generateReport')->name('admin.gen.report');
        Route::get('/rep/advert', 'AdminController@advert')->name('admin.advert');


        // get routes
        Route::get('/get/users', 'AdminController@getUsers');
        Route::get('/get/regions', 'AdminController@getRegion');
        Route::get('/get/districts', 'AdminController@getDistrict');
        Route::get('/get/zonals', 'AdminController@getZonal');
        Route::get('/get/posts', 'AdminController@getPost');
        Route::get('/get/messages', 'AdminController@getMessage');
        Route::get('/get/classes', 'AdminController@getClasses');
        Route::get('/get/subject', 'AdminController@getSubject');
        Route::get('/get/level', 'AdminController@getLevel');
        Route::get('/get/about', 'AdminController@getAbout');
        Route::get('/get/member', 'AdminController@getMember');
        Route::get('/get/carousel', 'AdminController@getCarousel');
        Route::get('/get/all_schools', 'AdminController@getSchools');
        Route::get('/get/all_users', 'AdminController@getAllUser');
        Route::get('/get/all_report', 'AdminController@getAllReport');
        Route::get('/get/all_advert', 'AdminController@getAdvert');


        // get with id
        Route::get('/get/comment/{id}', 'AdminController@getComment');
        Route::get('/get/gallery/{id}', 'AdminController@getGallery');
        Route::get('/update/message/{id}', 'AdminController@updateMessage');
        Route::get('/update/school/{id}', 'AdminController@updateSchoolMember');
        Route::get('/update/member/{id}', 'AdminController@updateSchoolGnaps');
        Route::get('/sch/schools/view/{id}', 'AdminController@viewSchoolDetail');
        Route::get('/membership/schools/{id}', 'AdminController@getMemberSchool');


        // save records
        Route::post('/save/user', 'AdminController@saveUsers');
        Route::post('/save/region', 'AdminController@saveRegion');
        Route::post('/save/district', 'AdminController@saveDistrict');
        Route::post('/save/zonal', 'AdminController@saveZonal');
        Route::post('/save/post', 'AdminController@savePost');
        Route::post('/save/classes', 'AdminController@saveClasses');
        Route::post('/save/subject', 'AdminController@saveSubject');
        Route::post('/save/level', 'AdminController@saveLevel');
        Route::post('/save/about', 'AdminController@saveAbout');
        Route::post('/save/carousel', 'AdminController@saveCarousel');
        Route::post('/save/owners', 'AdminController@saveSchoolOwner');
        Route::post('/save/report', 'AdminController@saveReport');
        Route::post('/save/advert', 'AdminController@saveAdvert');


        // update records
        Route::post('/update/user', 'AdminController@updateUsers');
        Route::post('/update/region', 'AdminController@updateRegion');
        Route::post('/update/district', 'AdminController@updateDistrict');
        Route::post('/update/zonal', 'AdminController@updateZonal');
        Route::post('/update/post', 'AdminController@updatePost');
        Route::post('/update/classes', 'AdminController@updateClasses');
        Route::post('/update/subject', 'AdminController@updateSubject');
        Route::post('/update/level', 'AdminController@updateLevel');
        Route::post('/update/profile', 'AdminController@updateProfile');
        Route::post('/update/password_change', 'AdminController@updateChangePassword');
        Route::post('/update/about', 'AdminController@updateAbout');
        Route::post('/update/carousel', 'AdminController@updateCarousel');
        Route::post('/update/owners', 'AdminController@updateSchoolOwner');
        Route::post('/update/school', 'AdminController@updateSchool');
        Route::post('/update/report', 'AdminController@updateReport');
        Route::post('/update/statistic/class', 'AdminController@updateStatisticClasses');
        Route::post('/update/head/info', 'AdminController@updateSchoolHead');
        Route::post('/update/school_register', 'AdminController@updateSchoolRegister');
        Route::post('/update/school_book', 'AdminController@updateSchoolTextBook');
        Route::post('/update/advert', 'AdminController@updateAdvert');


        // Delete records
        Route::delete('/delete/user/{id}', 'AdminController@deleteUser');
        Route::delete('/delete/region/{id}', 'AdminController@deleteRegion');
        Route::delete('/delete/district/{id}', 'AdminController@deleteDistrict');
        Route::delete('/delete/post/{id}', 'AdminController@deletePost');
        Route::delete('/delete/zonal/{id}', 'AdminController@deleteZonal');
        Route::delete('/delete/gallery/{id}', 'AdminController@deleteGallery');
        Route::delete('/delete/comment/{id}', 'AdminController@deleteComment');
        Route::delete('/delete/message/{id}', 'AdminController@deleteMessage');
        Route::delete('/delete/classes/{id}', 'AdminController@deleteClasses');
        Route::delete('/delete/subject/{id}', 'AdminController@deleteSubject');
        Route::delete('/delete/level/{id}', 'AdminController@deleteLevel');
        Route::delete('/delete/statistics/{id}', 'AdminController@deleteStatisticClass');
        Route::delete('/delete/head_info/{id}', 'AdminController@deleteHeadInfo');
        Route::delete('/delete/head_book/{id}', 'AdminController@deleteBookInfo');
        Route::delete('/delete/advert/{id}', 'AdminController@deleteAdvert');


        // View with ID
        Route::get('/view/school/{id}', 'AdminController@viewSchool');
        Route::get('/view/students/{id}', 'AdminController@viewStudents');
        Route::get('/view/head/{id}', 'AdminController@viewSchoolHead');
        Route::get('/view/register/{id}', 'AdminController@viewSchoolRegister');
        Route::get('/view/school_class/{id}', 'AdminController@viewSchoolClassLevels');
        Route::get('/view/school_book/{id}', 'AdminController@viewSchoolBook');
        Route::get('/view/all_schools', 'AdminController@viewAllSchools');
        Route::get('/view/all_school_group_region/{id}', 'AdminController@viewAllSchoolRegion');
        Route::get('/view/all_school_group_district/{id}/{dis}', 'AdminController@viewAllSchoolDistrict');
        Route::get('/view/all_school_group_zone/{id}/{dis}/{zone}', 'AdminController@viewAllSchoolZone');
        Route::get('/view/all_school_group/{id}', 'AdminController@viewAllSchoolGroup');
        Route::get('/view/all_school_stat', 'AdminController@getSchoolStatistic');
        Route::get('/publish/advert/{id}', 'AdminController@publishAdvert');
        Route::get('/decline/advert/{id}', 'AdminController@declineAdvert');
        Route::get('/carousel/active/{id}', 'AdminController@activeCarousel');
        Route::get('/carousel/hide/{id}', 'AdminController@hideCarousel');

    });

});













